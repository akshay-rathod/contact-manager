'use strict'
const mongoose = require('mongoose')

const Counter = new mongoose.Schema(
    {
        tag: {
            type: String,
            required: true
        },
        seq: {
            type: Number,
            required: true
        }
    },
    {
        timestamps: true,
        versionKey: false
    }
)

module.exports = mongoose.model('Counter', Counter)