'use strict'
const mongoose = require('mongoose')

const Contact = new mongoose.Schema(
    {
        ofUser: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            index: true
        },
        firstname: String,
        lastname: String,
        nickname: String,
        email: String,
        phone: String,
        city: String
    },
    {
        autoIndex: false
    }
)

module.exports = mongoose.model('Contact', Contact)