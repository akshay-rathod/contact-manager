'use strict'
const mongoose = require('mongoose')

const User = new mongoose.Schema(
    {
        details: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Contact'
        },
        username: String,
        password: String
    }
)

module.exports = mongoose.model('User', User)