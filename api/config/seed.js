'use strict'
const mongoose = require('mongoose')
const faker = require('faker')
const async = require('async')
const Counter = require('../models/counter')
const User = require('../models/user')
const Contact = require('../models//contact')

module.exports = (logger) => {
    Counter.find().count().then(count => {
        if (count > 0) {
            logger.info('Data already exists. Skipping seeding..')
        } else {
            logger.info('Generating mock data..')
            
            let contactCounter = new Counter(
                {
                    tag: 'contact',
                    seq: 0
                }
            )

            let userCounter = new Counter(
                {
                    tag: 'user',
                    seq: 0
                }
            )

            Counter.create(contactCounter, userCounter).then(_ => {
                logger.debug('Counters Initialized')

                let adminUser = new User(
                    {
                        username: 'akshay',
                        password: 'pass123'
                    }
                )

                let adminContact = new Contact(
                    {
                        ofUser: adminUser._id,
                        firstname: 'Akshay',
                        lastname: 'Rathod',
                        nickname: 'Aksh',
                        email: 'akshay.rathod60@gmail.com',
                        phone: '+91 855 486 1916',
                        city: 'Pune'
                    }
                )

                adminUser.details = adminContact._id

                User.create(adminUser).then(user => {
                    Contact.create(adminContact).then(contact => {
                        logger.debug(`Admin user initialized with credentials -> ${user.username} : ${user.password}`)

                        function createContact(data) {

                            return function(cb) {
                                Contact.create(data).then((contact, err) => {
                                    if (err) {
                                        logger.error('Error in creating contact: ', err)
                                        cb(err, null)
                                    } else {
                                        cb(null, contact)
                                    }
                                })
                            }
                        }

                        let seed = []

                        for (let i = 0; i < 10; i++) {
                            let card = faker.helpers.createCard()

                            let contact = new Contact(
                                {
                                    ofUser: user._id,
                                    firstname: card.name.split(' ')[0],
                                    lastname: card.name.split(' ')[1],
                                    nickname: card.username,
                                    email: card.email,
                                    phone: card.phone,
                                    city: card.address.city
                                }
                            )

                            seed.push(createContact(contact))                            
                        }

                        async.parallel(seed, err => {
                                if (err) logger.error('Error while generating mock data: ', err)
                                else logger.info('Finished creating mock data!')
                            }
                        )
                    })
                })
            })
        }
    })
}