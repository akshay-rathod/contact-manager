'use strict'
const env = require('dotenv').config()
const cors = require('cors')
const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const log4js = require('log4js')
const logger = log4js.getLogger()
logger.level = process.env.LOG_LEVEL

const app = express()
mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB}`,
    err => {
        if (err) logger.error('Failed to connect to MONGO: ', err)
        else logger.debug('Connected to MONGO')
    }
)

const seed = require('./config/seed')(logger)

app.use('*', cors())
app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.listen(process.env.PORT, _ => {
    logger.info(`API started on port: ${process.env.PORT}`)
})

app.get('/', (req, res) => {
    res.status(200).json(
        {
            message: `API is live on port: ${process.env.PORT}`,
            version: '1.0'
        }
    )
})

process.on('SIGINT', _ => {
    mongoose.connection.close(_ => {
        logger.info('Database connection closed.')
    })
})